/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <unistd.h>
#include "discovery_service.h"
#include "iot_gpio.h"
#include "ohos_init.h"
#include "os_adapter.h"
#include "securec.h"
#include "session.h"
#include "lwip/ip4_addr.h"
#include "lwip/netif.h"
#include "lwip/netifapi.h"
#include "wifi_device_config.h"
#include "wifi_hotspot.h"

static const char *g_demoModuleName = "com.huawei.communication.demo";
static const char *g_demoSessionName = "com.huawei.communication.demo.TurnOn";

#define LED_INTERVAL_TIME_US 300000
#define LED_TEST_GPIO 9

typedef enum {
    LED_ON = 0,
    LED_OFF,
    LED_SPAKE
} LedState;

static osThreadId_t g_ledCtrlTaskId = NULL;
static LedState g_ledState = LED_SPAKE;
extern int SoftApNetCfgStart(void);

void LedCtrolTaskProc(void)
{
    while (1) {
        if (g_ledState == LED_ON) {
            IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
        } else if (g_ledState == LED_OFF) {
            IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
        } else {
            printf("LED_SPAKE\n");
            IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
            usleep(LED_INTERVAL_TIME_US);
            IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
        }
        usleep(LED_INTERVAL_TIME_US);
    }
}

void CreateLedCtrlTask(void)
{
    if (g_ledCtrlTaskId != NULL) {
        return;
    }

    osThreadAttr_t attr;
    attr.name = "led_ctrl_task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = LOSCFG_BASE_CORE_TSK_DEFAULT_STACK_SIZE;
    attr.priority = osPriorityNormal4;

    g_ledCtrlTaskId = osThreadNew((osThreadFunc_t)LedCtrolTaskProc, NULL, &attr);
    if (g_ledCtrlTaskId == NULL) {
        printf("create task fail\n");
        return;
    }

    printf("CreateLedCtrlTask ok\n");
}

void OnBytesReceived(int32_t sessionId, const void *data, uint32_t dataLen)
{
    (void)sessionId;
    if (data == NULL || dataLen == 0) {
        printf("invalid param\n");
        return;
    }
    if (strcmp(data, "TurnOn") == 0) {
        g_ledState = LED_ON;
        printf("LED_ON\n");
        IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
    } else if (strcmp(data, "TurnOff") == 0) {
        g_ledState = LED_OFF;
        printf("LED_OFF\n");
        IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
    } else {
        printf("invalid cmd\n");
    }
}

void OnSessionClosed(int32_t sessionId)
{
    (void)sessionId;
}

int32_t OnSessionOpened(int32_t sessionId)
{
    printf("OnSessionOpened: sessionId = %d\n", sessionId);
    return 0;
}

static struct ISessionListener g_sessionCallback = {
    .onSessionOpened = OnSessionOpened,
    .onSessionClosed = OnSessionClosed,
    .onBytesReceived = OnBytesReceived,
};

static struct netif *g_lwipStaNetif = NULL;

static void WadStaResetAddr(struct netif *ptrLwipNetif)
{
    if (ptrLwipNetif == NULL) {
        printf("hisi_reset_addr::NULL param of netdev\n");
        return;
    }
    ip4_addr_t staGW;
    ip4_addr_t staIpaddr;
    ip4_addr_t staNetmask;

    IP4_ADDR(&staGW, 0, 0, 0, 0);
    IP4_ADDR(&staIpaddr, 0, 0,0, 0);
    IP4_ADDR(&staNetmask, 0, 0, 0, 0);
    netifapi_netif_set_addr(ptrLwipNetif, &staIpaddr, &staNetmask, &staGW);
}

static void WadWifiConnectionChangedHandler(int state, const WifiLinkedInfo *info)
{
    (void)info;
    if (state == WIFI_STATE_AVALIABLE) {
        printf("wifi:connected\n");
        netifapi_dhcp_start(g_lwipStaNetif);
    } else if (state == WIFI_STATE_NOT_AVALIABLE) {
        printf("wifi:disconnected\n");
        netifapi_dhcp_stop(g_lwipStaNetif);
        WadStaResetAddr(g_lwipStaNetif);
    }
}

WifiEvent g_eventHander = {0};

static int WadStaStart(void)
{
    WifiErrorCode error;
    error = EnableWifi();
    if (error != WIFI_SUCCESS) {
        printf("enablewifi failed, error = %d\n", error);
        return -1;
    }
    g_eventHander.OnWifiConnectionChanged = WadWifiConnectionChangedHandler;
    error = RegisterWifiEvent(&g_eventHander);
    if (error != WIFI_SUCCESS) {
        printf("RegisterWifiEvent failed, error = %d\n", error);
        return -1;
    }
    if (IsWifiActive() == 0) {
        printf("Wifi station is not active\n");
        return -1;
    }
    g_lwipStaNetif = netif_find("wlan0");
    if (g_lwipStaNetif == NULL) {
        printf("get netif failed\n");
        return -1;
    }
    return 0;
}

static int WadWapStaConnect(const char *ssid, const char *pwd)
{
    int netId = 0;
    WifiErrorCode error;
    WifiDeviceConfig config = {0};

    config.securityType = WIFI_SEC_TYPE_PSK;
    if (strcpy_s(config.ssid, sizeof(config.ssid), ssid) != 0) {
        return -1;
    }
    if (strcpy_s(config.preSharedKey, sizeof(config.preSharedKey), pwd) != 0) {
        return -1;
    }

    error = AddDeviceConfig(&config, &netId);
    if (error != WIFI_SUCCESS) {
        return -1;
    }

    error = ConnectTo(netId);
    if (error != WIFI_SUCCESS) {
        return -1;
    }
    return 0;
}

static int WadStaProc(const char *ssid, const char *pwd)
{
    int ret = WadStaStart();
    if (ret != 0) {
        return -1;
    }
    ret = WadWapStaConnect(ssid, pwd);
    if (ret != 0) {
        return -1;
    }
    return 0;
}

static void ConnectToAp(const char *ssid, const char *pwd)
{
    int ret = WadStaProc(ssid, pwd);
    if (ret != 0) {
        printf("WadStaPro failed\n");
        return;
    }
    for (int i = 0; i < 100; i++) {
        WifiLinkedInfo result = {0};
        (void)GetLinkedInfo(&result);
        if (result.connState == WIFI_CONNECTED) {
            printf("wifi connected\n");
            break;
        }
        sleep(5);
    }
}

void OnSuccess(int publishId)
{
    (void)publishId;
}

void OnFail(int publishId, PublishFailReason reason)
{
    (void)publishId;
    (void)reason;
}

static void InitSoftbus(void)
{
    static PublishInfo g_publishInfo = {
        .capabilityData = (unsigned char *)"1",
        .capability = "ddmpCapability",
        .dataLen = 1,
        .publishId = 1,
        .mode = DISCOVER_MODE_ACTIVE,
        .medium = COAP,
        .freq = MID,
    };
    static IPublishCallback g_publishCallback = {
        .onPublishSuccess = OnSuccess,
        .onPublishFail = OnFail,
    };

    int ret = PublishService(g_demoModuleName, &g_publishInfo, &g_publishCallback);
    if (ret != 0) {
        printf("PublishService err\n");
    }
    sleep(5);
    ret = CreateSessionServer(g_demoModuleName, g_demoSessionName, &g_sessionCallback);
    if (ret != 0) {
        printf("CreateSessionServer err\n");
    }
    printf("InitSoftbus ok\n");
}

static void InitLed(void)
{
    IoTGpioInit(LED_TEST_GPIO);
    IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
    IoTGpioSetDir(LED_TEST_GPIO, IOT_GPIO_DIR_OUT);
    printf("InitLed ok\n");
}

void TestLedCtrlDemoInit(void)
{
    printf("TestLedCtrlDemoInit begin\n");
    InitLed();
    CreateLedCtrlTask();
    ConnectToAp("softbus", "12345678");
    InitSoftbus();
    SoftApNetCfgStart();
    g_ledState = LED_ON;
    printf("TestLedCtrlDemoInit ok\n");
}

SYS_SERVICE_INIT_PRI(TestLedCtrlDemoInit, 4);
