/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "netcfg_udp.h"

#include <string.h>
#include "fcntl.h"
#include "securec.h"

#include "lwip/inet.h"
#include "lwip/sockets.h"

#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#define MULTICAST_GROUP_ADDR4 "238.238.238.238"
#define RECEIVE_IP_LEN 16

static void CloseFd(int fd)
{
    if (fd >= 0) {
        closesocket(fd);
    }
}

int WifiUdpNew(unsigned short port)
{
    struct sockaddr_in servAddr;
    struct ip_mreq group;
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0) {
        return -1;
    }

    int flags = fcntl(fd, F_GETFL, 0);
    if (flags < 0) {
        CloseFd(fd);
        return -1;
    }

    if (port != 0) {
        int opt = 1;
        if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const char *)&opt, sizeof(opt)) != 0) {
            CloseFd(fd);
            return -1;
        }

        (void)memset_s((void *)&servAddr, sizeof(struct sockaddr_in), 0, sizeof(struct sockaddr_in));

        servAddr.sin_family = AF_INET;
        servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servAddr.sin_port = htons(port);
        if (bind(fd, (struct sockaddr *)&servAddr, sizeof(struct sockaddr_in)) < 0) {
            CloseFd(fd);
            return -1;
        }

        (void)memset_s((void *)&group, sizeof(struct ip_mreq), 0, sizeof(struct ip_mreq));
        group.imr_multiaddr.s_addr = inet_addr(MULTICAST_GROUP_ADDR4);
        group.imr_interface.s_addr = htonl(INADDR_ANY);
        if (setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &group, sizeof(group)) < 0) {
            CloseFd(fd);
            return -1;
        }
        opt = 1;
        if (setsockopt(fd, SOL_SOCKET, SO_BROADCAST, (const void *)&opt, sizeof(opt)) < 0) {
            CloseFd(fd);
            return -1;
        }
    }
    return fd;
}

void WifiUdpSend(int fd, const char *buf, unsigned short len,
    const char *receiveIp, unsigned short receivePort)
{
    struct sockaddr_in dstAddr;
    if (buf == NULL || receiveIp == NULL) {
        printf("udp send bad args\n");
        return;
    }
    (void)memset_s((void *)&dstAddr, sizeof(struct sockaddr_in), 0, sizeof(struct sockaddr_in));

    dstAddr.sin_addr.s_addr = inet_addr(receiveIp);
    dstAddr.sin_port = htons(receivePort);
    dstAddr.sin_family = AF_INET;

    int ret = (int)(sendto(fd, buf, len, 0, (struct sockaddr *)&dstAddr, sizeof(struct sockaddr_in)));

    if (ret < 0) {
        printf("udp send failed");
    }
}

int WifiUdpRead(int fd, char *buf, unsigned short len,
    char *receiveIp, unsigned short *receivePort)
{
    struct sockaddr_in dstAddr;
    unsigned int addrLen = sizeof(dstAddr);
    if (buf == NULL) {
        printf("udp read bad args\n");
        return -1;
    }
    (void)memset_s((void *)&(dstAddr), addrLen, 0, addrLen);
    int length = (int)(recvfrom(fd, buf, len, 0, (struct sockaddr *)&dstAddr, &addrLen));
    if (length <= 0) {
        printf("udp read failed.\n");
        return -1;
    }

    if (length < len) {
        buf[length] = '\0';
    }

    if (receiveIp == NULL || receivePort == NULL) {
        return length;
    }
    char *dottedIp = inet_ntoa(dstAddr.sin_addr);
    if (dottedIp == NULL) {
        printf("udp read addr format wrong\n");
        return -1;
    }
    if (strncpy_s(receiveIp, RECEIVE_IP_LEN, dottedIp, strlen(dottedIp)) != EOK) {
        printf("udp read copy failed\n");
        return -1;
    }
    *receivePort = ntohs(dstAddr.sin_port);
    return length;
}

static int WifiUdpRemoveMultiGroup(int fd)
{
    struct ip_mreq group;
    (void)memset_s((void *)&group, sizeof(struct ip_mreq), 0, sizeof(struct ip_mreq));
    group.imr_multiaddr.s_addr = inet_addr(MULTICAST_GROUP_ADDR4);
    group.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(fd, IPPROTO_IP, IP_DROP_MEMBERSHIP, &group, sizeof(group)) < 0) {
        printf("remove from multi group failed\n");
        return -1;
    }
    return 0;
}

void WifiUdpRemove(int fd)
{
    if (fd < 0) {
        return;
    }
    if (WifiUdpRemoveMultiGroup(fd) != 0) {
        printf("remove from multi group failed\n");
    }
    CloseFd(fd);
}