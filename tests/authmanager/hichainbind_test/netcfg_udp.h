/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NETCFG_UDP_H
#define NETCFG_UDP_H

int WifiUdpNew(unsigned short port);
void WifiUdpSend(int fd, const char *buf, unsigned short len,
    const char *receiveIp, unsigned short receivePort);
int WifiUdpRead(int fd, char *buf, unsigned short len,
    char *receiveIp, unsigned short *receivePort);
void WifiUdpRemove(int fd);

#endif // NETCFG_UDP_H