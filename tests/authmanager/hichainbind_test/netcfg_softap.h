/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NETCFG_SOFTAP_H
#define NETCFG_SOFTAP_H

#include <stddef.h>
#include "cmsis_os2.h"

#define NETWORK_CONFIG_PORT 5683
#define MAX_MSG_BUFF 1024
#define IP_LEN 16
#define SOFT_AP_TASK_STACK_SIZE 3072
#define SOFT_AP_TASK_PRIO 25
#define PAKE_SESSION_KEY_LEN 16
#define NETCFG_HEAD_LEN 2
#define NETCFG_MSG_LEN (MAX_MSG_BUFF - NETCFG_HEAD_LEN)
#define NETCFG_DEFAULT_ID "default"
#define NETCFG_DEFAULT_ID_LEN 7
#define NETCFG_PRODUCTID_LEN 4
#define NETCFG_SN_LEN 32
#define NETCFG_DEVICD_LEN (NETCFG_PRODUCTID_LEN + NETCFG_SN_LEN + 30)

typedef enum {
    SOFT_AP_NETCFG_STATE_RESET = 0,
    SOFT_AP_NETCFG_STATE_WAITING,
    SOFT_AP_NETCFG_STATE_NEGOTIATION,
    SOFT_AP_NETCFG_STATE_HEARTBEAT,
    SOFT_AP_NETCFG_STATE_KEY_PARSING,
    SOFT_AP_NETCFG_STATE_EXIT,
    SOFT_AP_NETCFG_STATE_NETCONFIG_RESULT,
    SOFT_AP_NETCFG_STATE_CTRL,
    SOFT_AP_NETCFG_STATE_END,
} SoftApNetCfgState;

struct NetCfgSoftApMessage {
    unsigned short head;
    unsigned char msg[MAX_MSG_BUFF - NETCFG_HEAD_LEN];
    unsigned short len;
};

struct NetCfgSoftApStateMachine {
    SoftApNetCfgState state;
    struct NetCfgSoftApMessage readMsgBuff;
    struct NetCfgSoftApMessage sendMsgBuff;
    unsigned short sendMsgLength;
    unsigned short readMsgLength;

    char sessionKey[PAKE_SESSION_KEY_LEN];
    char remoteIp[IP_LEN];
    unsigned short remotePort;
    signed int serviceResult;

    struct hc_handle *pakeServer;
    struct session_identity *serverIdentity;
    struct hc_call_back *hiChainCallback;

    int ctrMsgIndex;
};

int SoftApNetCfgStart(void);
void SoftApNetCfgStop(void);

#endif // NETCFG_SOFTAP_H