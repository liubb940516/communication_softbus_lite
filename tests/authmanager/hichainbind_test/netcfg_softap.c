/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 #include "netcfg_softap.h"

 #include "hichain.h"
 #include "securec.h"
 #include "netcfg_udp.h"
 #include "lwip/sockets.h"
 #include "lwip/netif.h"
 #include "lwip/netifapi.h"
 #include <los_task.h>

#ifdef SOFTBUS_DEBUG
#include "common_info_manager.h"
extern char g_peerAuthId[MAX_AUTH_ID_LEN];
#endif

#define HICHAIN_PIN "softbus"
#define PEER_DEVICE_ID "7A1EAB2C4FF6DE75A136D3862AB2B0CB35EE81823B83C407487EC2E3AB091DC5"
extern int ReadDeviceId(char *deviceId, unsigned int len);
static void SoftApNetCfgStartThread(void);

static struct NetCfgSoftApStateMachine *g_softApState = NULL;
struct hc_pin g_netCfgPin = {0};
static int g_udpFd = -1;

static void ResetStateMachine(void)
{
    if (g_softApState->pakeServer != NULL) {
        destroy((hc_handle)(g_softApState->pakeServer));
    }
    if (g_softApState->serverIdentity != NULL) {
        free(g_softApState->serverIdentity);
    }
    if (g_softApState->hiChainCallback != NULL) {
        free(g_softApState->hiChainCallback);
    }
    g_softApState->pakeServer = NULL;
    g_softApState->serverIdentity = NULL;
    g_softApState->hiChainCallback = NULL;
    g_softApState->state = SOFT_AP_NETCFG_STATE_NEGOTIATION;

    printf("NetCfgSoftAp mode to RESET!\n");
}

static void SendMsg(unsigned short head, const char *msg, unsigned short len)
{
    if (msg != NULL && len != 0) {
        if (memcpy_s(g_softApState->sendMsgBuff.msg, NETCFG_MSG_LEN, msg, len) != EOK) {
            return;
        }
    }
    g_softApState->sendMsgBuff.head = htons(head);
    g_softApState->sendMsgBuff.len = len;
    g_softApState->sendMsgLength = len + NETCFG_HEAD_LEN;
    WifiUdpSend(g_udpFd, (char *)&(g_softApState->sendMsgBuff), g_softApState->sendMsgLength,
        g_softApState->remoteIp, g_softApState->remotePort);
}

void NetCfgTransmit(const struct session_identity *identity, const void *data, uint32_t length)
{
     printf("[bind device] NetCfgTransmit\n");
    (void)identity;
    SendMsg(SOFT_AP_NETCFG_STATE_NEGOTIATION, (char *)data, length);
}

void GetProtocolParams(const struct session_identity *identity, int32_t operationCode,
    struct hc_pin *pin, struct operation_parameter *para)
{
    printf("[bind device] GetProtocolParams\n");
    (void)identity;
    (void)operationCode;
    struct hc_auth_id peer_defaultAuthId = {strlen(PEER_DEVICE_ID), PEER_DEVICE_ID};
    *pin = g_netCfgPin;
    para->key_length = PAKE_SESSION_KEY_LEN;
    para->peer_auth_id = peer_defaultAuthId;

    #ifdef SOFTBUS_DEBUG
    if (strlen(g_peerAuthId) == 0) {
        printf("GetProtocolParams g_peerAuthId is NULL\n");
    }
    memcpy_s(para->peer_auth_id.auth_id, HC_AUTH_ID_BUFF_LEN, g_peerAuthId, strlen(g_peerAuthId));
    #endif

    ReadDeviceId((char *)para->self_auth_id.auth_id, 64);
    para->self_auth_id.length = 64;

    printf("[bind device] GetProtocolParams peer auth id = %s\n", para->peer_auth_id.auth_id);
    printf("[bind device] GetProtocolParams self auth id = %s\n", para->self_auth_id.auth_id);
}

void SetSessionKey(const struct session_identity *identity, const struct hc_session_key *session)
{
    printf("[bind device] SetSessionKey\n");
    (void)identity;
    memcpy_s(g_softApState->sessionKey, sizeof(g_softApState->sessionKey), session->session_key, session->length);
}

void SetServiceResult(const struct session_identity *identity, int32_t result)
{
    (void)identity;
    g_softApState->serviceResult = result;
    printf("[bind device] SetServiceResult result = %d\n", result);
    if (result == END_SUCCESS || result == END_FAILED) {
        ResetStateMachine();
    }
}

int32_t ConfirmReceiveRequest(const struct session_identity *identity, int32_t operationCode)
{
    (void)identity;
    (void)operationCode;
    return 0;
}

static void InitUdpMulticast(void)
{
    g_udpFd = WifiUdpNew(NETWORK_CONFIG_PORT);
}

static int InitHiChain(void)
{
    struct session_identity serverIdentity = {
        0,
        {NETCFG_DEFAULT_ID_LEN, NETCFG_DEFAULT_ID},
        {NETCFG_DEFAULT_ID_LEN, NETCFG_DEFAULT_ID},
        0
    };

    struct hc_call_back hiChainCallback = {
        NetCfgTransmit,
        GetProtocolParams,
        SetSessionKey,
        SetServiceResult,
        ConfirmReceiveRequest
    };

    g_softApState->serverIdentity = calloc(1, sizeof(struct session_identity));
    memcpy_s(g_softApState->serverIdentity, sizeof(struct session_identity), &serverIdentity, sizeof(struct session_identity));

    g_softApState->hiChainCallback = calloc(1, sizeof(struct hc_call_back));
    memcpy_s(g_softApState->hiChainCallback, sizeof(struct hc_call_back), &hiChainCallback, sizeof(struct hc_call_back));

    g_softApState->pakeServer = get_instance(g_softApState->serverIdentity, HC_ACCESSORY, g_softApState->hiChainCallback);

    if (g_softApState->serverIdentity == NULL || g_softApState->hiChainCallback == NULL ||
        g_softApState->pakeServer == NULL) {
        printf("Hichain init failed\n");
        return -1;
    }

    printf("Init Hichain ok\n");
    return 0;
}

static void Negotiate(void)
{
    if (g_softApState->pakeServer == NULL) {
        if (InitHiChain() != 0) {
            ResetStateMachine();
            return;
        }
    }
    struct uint8_buff request = {
        (uint8_t *)&(g_softApState->readMsgBuff.msg),
        g_softApState->readMsgBuff.len,
        g_softApState->readMsgBuff.len
    };
    receive_data(g_softApState->pakeServer, &request);
}

static int IsIllegalClient(char *ip, unsigned short port)
{
    if (g_softApState->remotePort != port) {
        return -1;
    }
    if (strcmp(g_softApState->remoteIp, ip) != 0) {
        return -1;
    }
    return 0;
}

static int CheckIfNeedReSendMsg(void)
{
    if (g_softApState->readMsgBuff.head == g_softApState->state) {
        return 0;
    }
    if ((g_softApState->state == SOFT_AP_NETCFG_STATE_CTRL) ||
        (g_softApState->readMsgBuff.head == g_softApState->ctrMsgIndex)) {
        return 0;
    }
    return -1;
}

static int WaitNetMessage(void)
{
    printf("WaitNetMessage begin\n");

    char remoteIp[IP_LEN] = {0};
    unsigned short remotePort;
    memset_s(&(g_softApState->readMsgBuff), sizeof(struct NetCfgSoftApMessage), 0, sizeof(struct NetCfgSoftApMessage));
    g_softApState->readMsgLength = WifiUdpRead(g_udpFd, (char *)(&(g_softApState->readMsgBuff)), MAX_MSG_BUFF, remoteIp, &remotePort);
    g_softApState->readMsgBuff.len = g_softApState->readMsgLength - NETCFG_HEAD_LEN;
    g_softApState->readMsgBuff.head = ntohs(g_softApState->readMsgBuff.head);
    if (g_softApState->readMsgLength < NETCFG_HEAD_LEN) {
        printf("WaitNetMessage readMsgLength < NETCFG_HEAD_LEN\n");
        return -1;
    }

    g_softApState->remotePort = remotePort;
    if (strncpy_s(g_softApState->remoteIp, sizeof(g_softApState->remoteIp), remoteIp, strlen(remoteIp)) != EOK) {
        printf("get remoteIp failed\n");
        return -1;
    }
    if (IsIllegalClient(remoteIp, remotePort)) {
        printf("wait next message illegal client\n");
        return -1;
    }
    if (CheckIfNeedReSendMsg()) {
        printf("resend last msg\n");
        WifiUdpSend(g_udpFd, (char *)(&(g_softApState->sendMsgBuff)), g_softApState->sendMsgLength,
            g_softApState->remoteIp, g_softApState->remotePort);
        return -1;
    }
    printf("receive msg\n");
    return 0;
}

static void ProcessNextMessage(void)
{
    switch(g_softApState->state) {
        case SOFT_AP_NETCFG_STATE_NEGOTIATION:
            Negotiate();
            break;
        default:
            break;
    }
}

static void SoftApNetCfgThread(void)
{
    while (g_softApState != NULL) {
        if (WaitNetMessage()) {
            continue;
        }
        ProcessNextMessage();
    }
}

static int g_taskCreateFlag = 0;
static osThreadId_t g_netCfgThread = NULL;

static void SoftApNetCfgStartThread(void)
{
    if (g_netCfgThread != NULL) {
        return;
    }

    osThreadAttr_t attr;
    attr.name = "SoftbusHiChainBinTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = SOFT_AP_TASK_STACK_SIZE;
    attr.priority = SOFT_AP_TASK_PRIO;

    g_netCfgThread = osThreadNew((osThreadFunc_t)SoftApNetCfgThread, NULL, &attr);
    if (g_netCfgThread == NULL) {
        printf("failed to create task\n");
        g_taskCreateFlag = 0;
    }
    g_taskCreateFlag = 1;
    printf("SoftApNetCfgStartThread ok\n");
}

static void SoftApNetCfgStopThread(void)
{
    if (g_netCfgThread != NULL) {
        osThreadTerminate(g_netCfgThread);
        g_netCfgThread = NULL;
    }

    g_taskCreateFlag = 0;
}

int SoftApNetCfgStart(void)
{
    if (g_softApState != NULL) {
        printf("softap netcfg is now running\n");
        return -1;
    }
    if (g_taskCreateFlag == 1) {
        printf("invalid args\n");
        return -1;
    }
    g_softApState = calloc(1, sizeof(struct NetCfgSoftApStateMachine));
    if (g_softApState == NULL) {
        printf("init state machine failed\n");
        return -1;
    }
    g_softApState->state = SOFT_AP_NETCFG_STATE_NEGOTIATION;
    g_netCfgPin.length = strlen(HICHAIN_PIN);
    int ret = memcpy_s(g_netCfgPin.pin, sizeof(g_netCfgPin.pin), HICHAIN_PIN, strlen(HICHAIN_PIN) + 1);
    if (ret != 0) {
        free(g_softApState);
        g_softApState = NULL;
        return -1;
    }

    InitUdpMulticast();
    SoftApNetCfgStartThread();

    printf("SoftApNetCfgStart ok\n");
    return 0;
}

void SoftApNetCfgStop(void)
{
    printf("stop and exit\n");
    if (g_softApState == NULL) {
        return;
    }
    ResetStateMachine();
    if (g_softApState != NULL) {
        free(g_softApState);
        g_softApState = NULL;
    }

    SoftApNetCfgStopThread();
    WifiUdpRemove(g_udpFd);
    g_udpFd = -1;
}
